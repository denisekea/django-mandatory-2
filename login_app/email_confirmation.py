from django.core.mail import send_mail


def email_confirmation(message_dict):
    contents = f"""
   Hello, thank you for creating an account in our bank.
   """
    send_mail(
        'Signup Confirmation',
        contents,
        'magd0780@stud.kea.dk',
        [message_dict['email']],
        fail_silently=False
    )
