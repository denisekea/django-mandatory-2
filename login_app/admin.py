from . models import UserProfile
from django.contrib import admin
# Register your models here.
from .models import PasswordResetRequest


admin.site.register(PasswordResetRequest)
admin.site.register(UserProfile)
