# file: todo_project/login_app/models.py

from django.db import models
from django.contrib.auth.models import User
from secrets import token_urlsafe


class UserProfile(models.Model):
    RANK_CHOICES = (
        ('basic', 'basic'),
        ('silver', 'silver'),
        ('gold', 'gold')
    )
    RankChoices = models.TextChoices('RankChoices', 'basic silver gold')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=25, blank=True)
    rank = models.CharField(
        max_length=30, choices=RANK_CHOICES, default='basic')

    def __str__(self):
        return self.user.username


class PasswordResetRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=43, default=token_urlsafe)
    created_timestamp = models.DateTimeField(auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user} - {self.created_timestamp} - {self.updated_timestamp} - {self.token}'
