from django.core.mail import send_mail


def email_password(message_dict):
    contents = f"""
   Hello,
   Here is your secret code: {message_dict['token']}
   """
    send_mail(
        'Password Reset',
        contents,
        'magd0780@stud.kea.dk',
        [message_dict['email']],
        fail_silently=False
    )
