from django.db import models
from django.contrib.auth.models import User
import random


def random_string():
    return str(random.randint(10000000000000, 99999999999999))


class CustomerChat(models.Model):
    chat_id = models.AutoField(primary_key=True, default=random_string)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.CharField(max_length=500, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.chat_id} - {self.message} - {self.user.username} - {self.timestamp}'
