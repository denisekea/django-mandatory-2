from django.shortcuts import render
import requests
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import ChatSerializer
from .models import CustomerChat


@api_view(['GET'])
def api_overview(request):
    api_urls = {
        'Chatlog': '/chatlog',
        'User chatlog': '/chatlog/<str:username>',
        'Create chatlog': '/chatlog-create'
    }

    return Response(api_urls)


@api_view(['GET'])
def chatlog(request):
    chats = CustomerChat.objects.all()
    serializer = ChatSerializer(chats, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def chatlog_user(request, username):
    print(username)
    chats = CustomerChat.objects.filter(user__username__iexact=username)
    serializer = ChatSerializer(chats, many=True)
    return Response(serializer.data)


@api_view(['POST'])
def chatlog_create(request):
    serializer = ChatSerializer(data=request.data)

    if serializer.is_valid():
        serializer.save()

    return Response(serializer.data)