from rest_framework import serializers
from .models import CustomerChat


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerChat
        fields = '__all__'
