from django.urls import path
from . import views
from . import api

app_name = 'chat_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:room_name>/', views.room, name='room'),
    path('api/overview', api.api_overview, name='api-overview'),
    path('api/chatlog', api.chatlog, name='chatlog'),
    path('api/chatlog/<str:username>', api.chatlog_user, name='chatlog-user'),
    path('api/chatlog-create', api.chatlog_create, name='chatlog-create'),
]
