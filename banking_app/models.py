from django.db import models
from django.contrib.auth.models import User
import uuid
import random


def random_string():
    return str(random.randint(100000000000, 999999999999))


class BankAccount(models.Model):
    account_no = models.AutoField(primary_key=True, default=random_string)
    balance = models.DecimalField(max_digits=20, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.account_no} - {self.balance} - {self.user.username}'


class Loan(models.Model):
    loan_no = models.AutoField(primary_key=True, default=random_string)
    total_loan = models.DecimalField(max_digits=20, decimal_places=2)
    balance = models.DecimalField(max_digits=20, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.loan_no} - {self.total_loan} - {self.balance} - {self.user.username}'


class Transaction(models.Model):
    transaction_id = models.UUIDField(
        primary_key=True, max_length=30, default=uuid.uuid4, editable=False)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    from_account = models.ForeignKey(
        'BankAccount', on_delete=models.CASCADE, related_name='from_account', null=True)
    to_account = models.ForeignKey(
        'BankAccount', on_delete=models.CASCADE, related_name='to_account', null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'amount {self.amount} - {self.timestamp} - {self.transaction_id}'


class LoanPayment(models.Model):
    transaction_id = models.UUIDField(
        primary_key=True, max_length=30, default=uuid.uuid4, editable=False)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    from_account = models.ForeignKey(
        'BankAccount', on_delete=models.CASCADE, null=True)
    to_loan = models.ForeignKey('Loan', on_delete=models.CASCADE, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f'amount {self.amount} - {self.timestamp} - {self.transaction_id}'
