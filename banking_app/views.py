from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from decimal import Decimal
from django.contrib.auth.decorators import login_required
from .models import BankAccount, Loan, Transaction, LoanPayment
from login_app.models import UserProfile
from django.contrib.auth.models import User
from . logout_time import logout_time
from datetime import datetime, timedelta
from django.conf import settings


@login_required(login_url='/accounts/login/')
def index(request):
    bankaccounts = BankAccount.objects.filter(user=request.user)
    loans = Loan.objects.filter(user=request.user)
    session_time = logout_time(request)
    user = request.user
    u = UserProfile.objects.get(user=user)
    user_rank = u.rank
    print(user_rank)
    context = {'bankaccounts': bankaccounts,
               'loans': loans, 'userrank': user_rank, 'session_time': session_time}

    if request.method == 'POST':
        loan_amount = request.POST['loan_amount']
        new_loan = Loan()
        new_loan.total_loan = loan_amount
        new_loan.balance = loan_amount
        new_loan.user = request.user
        new_loan.save()

    return render(request, 'banking_app/index.html', context)


@login_required(login_url='/accounts/login/')
def transfer(request):
    allUserAccounts = BankAccount.objects.filter(user=request.user)
    allAccounts = BankAccount.objects.all()
    session_time = logout_time(request)
    user = request.user
    u = UserProfile.objects.get(user=user)
    user_rank = u.rank

    if request.method == 'POST':
        amount = request.POST['amount']
        from_account = request.POST['from_account']
        to_account = request.POST['to_account']

        from_account_details = get_object_or_404(
            BankAccount, account_no=from_account)
        to_account_details = get_object_or_404(
            BankAccount, account_no=to_account)

        if Decimal(amount) < 0.01:
            context = {
                'message': 'Please enter an amount higher than zero.', 'userrank': user_rank
            }
            return render(request, 'banking_app/confirmation.html', context)

        if from_account == to_account:
            context = {
                'message': 'Cannot transfer to the same account. Try again.', 'userrank': user_rank
            }
            return render(request, 'banking_app/confirmation.html', context)

        if from_account_details.balance < Decimal(amount):
            context = {
                'message': 'Insufficient funds on account.', 'userrank': user_rank
            }
            return render(request, 'banking_app/confirmation.html', context)

        # calculate new balances
        from_account_details.balance = from_account_details.balance - \
            Decimal(amount)
        from_account_details.save()
        to_account_details.balance = to_account_details.balance + \
            Decimal(amount)
        to_account_details.save()

        # create new transaction
        new_transaction = Transaction()
        new_transaction.amount = Decimal(amount)
        new_transaction.from_account = from_account_details
        new_transaction.to_account = to_account_details
        new_transaction.user = request.user
        new_transaction.save()

        context = {
            'message': 'Successfully made the transfer.', 'userrank': user_rank
        }
        return render(request, 'banking_app/confirmation.html', context)

    context = {
        'allUserAccounts': allUserAccounts,
        'allAccounts': allAccounts,
        'userrank': user_rank,
        'session_time': session_time
    }
    return render(request, 'banking_app/transfer.html', context)


@login_required(login_url='/accounts/login/')
def loan_payment(request):
    allUserAccounts = BankAccount.objects.filter(user=request.user)
    allUserLoans = Loan.objects.filter(user=request.user)
    session_time = logout_time(request)
    if request.method == 'POST':
        amount = request.POST['amount']
        from_account = request.POST['from_account']
        to_loan = request.POST['to_loan']

        from_account_details = get_object_or_404(
            BankAccount, account_no=from_account)
        to_loan_details = get_object_or_404(Loan, loan_no=to_loan)

        if Decimal(amount) < 0.01:
            context = {
                'message': 'Please enter an amount higher than zero.'
            }
            return render(request, 'banking_app/confirmation.html', context)

        if from_account_details.balance < Decimal(amount):
            context = {
                'message': 'Insufficient funds on account.'
            }
            return render(request, 'banking_app/confirmation.html', context)

        # calculate new balances
        from_account_details.balance = from_account_details.balance - \
            Decimal(amount)
        from_account_details.save()
        to_loan_details.balance = to_loan_details.balance - Decimal(amount)
        to_loan_details.save()

        # create new payment
        new_payment = LoanPayment()
        new_payment.amount = Decimal(amount)
        new_payment.from_account = from_account_details
        new_payment.to_loan = to_loan_details
        new_payment.user = request.user
        new_payment.save()

        context = {
            'message': 'Successfully made the payment.'
        }
        return render(request, 'banking_app/confirmation.html', context)

    context = {
        'allUserAccounts': allUserAccounts,
        'allUserLoans': allUserLoans,
        'session_time': session_time
    }
    return render(request, 'banking_app/loanpayment.html', context)


@login_required(login_url='/accounts/login/')
def bankaccount_details(request, accountno):
    user = request.user
    u = UserProfile.objects.get(user=user)
    user_rank = u.rank
    if BankAccount.objects.filter(account_no=accountno).exists():
        account = get_object_or_404(BankAccount, account_no=accountno)

    transactions = Transaction.objects.filter(
        Q(from_account=accountno) | Q(to_account=accountno))
    payments = LoanPayment.objects.filter(from_account=accountno)
    alltransactions = transactions.union(payments).order_by('-timestamp')

    session_time = logout_time(request)

    context = {
        'account': account,
        'transactions': alltransactions,
        'userrank': user_rank,
        'session_time': session_time
    }
    return render(request, 'banking_app/bankaccount.html', context)


@login_required(login_url='/accounts/login/')
def loan_details(request, loanno):
    if Loan.objects.filter(loan_no=loanno).exists():
        loan = get_object_or_404(Loan, loan_no=loanno)

    loanpayments = LoanPayment.objects.filter(
        to_loan=loanno).order_by('-timestamp')

    session_time = logout_time(request)

    context = {
        'loan': loan,
        'loanpayments': loanpayments,
        'session_time': session_time
    }
    return render(request, 'banking_app/loan.html', context)


@login_required(login_url='/accounts/login/')
def profile(request):
    user = request.user
    user_profile = UserProfile.objects.get(user=user)
    session_time = logout_time(request)
    context = {
        'user': user, 'userprofile': user_profile, 'session_time': session_time
    }
    return render(request, 'banking_app/profile.html', context)
