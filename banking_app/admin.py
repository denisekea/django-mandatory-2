from django.contrib import admin
from .models import BankAccount, Loan, Transaction, LoanPayment


admin.site.register(BankAccount)
admin.site.register(Loan)
admin.site.register(Transaction)
admin.site.register(LoanPayment)