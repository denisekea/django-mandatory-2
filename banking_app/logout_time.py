from datetime import datetime, timedelta
from django.conf import settings


def logout_time(request):

    last_touch_string = request.session['last_touch']
    last_touch_date = datetime.strptime(
        last_touch_string, '%Y-%m-%d %H:%M:%S')
    session_time = (last_touch_date + timedelta(0,
                                                settings.AUTO_LOGOUT_DELAY * 60, 0))
    return session_time
