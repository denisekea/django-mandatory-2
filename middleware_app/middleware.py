from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib.auth import logout
from datetime import datetime, timedelta
from django.contrib.auth.models import User


class IPFilterMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        allowed_ip_addresses = settings.IPFILTER_MIDDLEWARE['ALLOWED_IP_ADDRESSES']
        client_ip_address = request.META.get('REMOTE_ADDR')
        print(f"** client ip address: {client_ip_address}")

        if not client_ip_address in allowed_ip_addresses:
            raise PermissionDenied

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        response['X-IP-FILTER'] = 'IP FILTER BY KEA'

        return response


class AutoLogout:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):

        if not request.user.is_authenticated:
            return self.get_response(request)

        try:
            time = datetime.now()
            last_touch_string = request.session['last_touch']

            last_touch_date = datetime.strptime(
                last_touch_string, '%Y-%m-%d %H:%M:%S')

            if time - last_touch_date > (timedelta(0, settings.AUTO_LOGOUT_DELAY * 60, 0)):
                print("User Auto Logged Out")
                logout(request)
                del request.session['last_touch']

                return self.get_response(request)
            else:
                return self.get_response(request)

        except KeyError:
            time_now = datetime.now()
            formatedDate = time_now.strftime("%Y-%m-%d %H:%M:%S")
            request.session['last_touch'] = formatedDate

        return self.get_response(request)
