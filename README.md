# django-mandatory1

## ER Diagram

[See ERD on LucidChart](https://app.lucidchart.com/lucidchart/5dcd34b2-350a-41cf-894a-7e1d7f9002f1/view)

## Start virtual environment

After you started the venv, install project's pip packages in the project directory:

`pip install -r requirements.txt`

## Migrate project

`python manage.py makemigrations`

`python manage.py migrate`

## Redis Server

To run our project with full functionality you need to run Redis Server first. 
To make it happen - you need to install Docker platform. After that you can run Redis Server with command:

`docker run -p 6379:6379 redis`

Next in an active Django environment run command: 

`python manage.py rqworker`

## Run project

`python manage.py runserver`

## Login to banking app

Go to: `localhost:8000`

username: Henrik <br />
password: kea123

Basic user:

username: Denny <br />
password: kea12345

## Login as bank employee / admin

Go to: `localhost:8000/admin`

username: Henrik <br />
password: kea123

## Chatlog API endpoints

This API can be used to access the customers' chatlogs in another app (e.g. a customer service or analytics tool).

### Overview endpoints - GET

`localhost:8000/help/api/overview`

### All customer chatlogs - GET

`localhost:8000/help/api/chatlog`

### Chatlogs by username - GET

`localhost:8000/help/api/chatlog/<username>`

### Create new chatlog - POST

`localhost:8000/help/api/chatlog-create`

POST JSON object with a userid and a message string.
Used every time the user sends a chat message.

```
{
    "user": "21",
    "message": "Hi, I'm a chat message."
}

```
